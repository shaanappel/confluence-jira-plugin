- For CLOUD: https://stash.atlassian.com/projects/CONFCLOUD/repos/confluence-jira-plugin
- For SERVER: use this repository

How to run the WebDriver test in the DEV env,

#1 - Launch a JIRA instance, for example, "atlas-run-standalone --container tomcat8x --product jira --version 7.2.1 --data-version 7.2.1"
#2 - Add a new project having the key of "TP", and create two issues, namely TP-1 and TP-2
#3 - Configure the IDE by referring to the JIRA instance with the system property - "baseurl.jira". For example
     -Dbaseurl.jira=http://localhost:2990/jira

Now, the WebDriver tests should be able to be executed from IDE.
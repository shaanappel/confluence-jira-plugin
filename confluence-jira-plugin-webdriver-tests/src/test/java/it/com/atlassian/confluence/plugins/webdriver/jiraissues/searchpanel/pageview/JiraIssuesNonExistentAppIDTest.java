package it.com.atlassian.confluence.plugins.webdriver.jiraissues.searchpanel.pageview;

import com.atlassian.confluence.test.rpc.api.permissions.GlobalPermission;
import com.atlassian.confluence.test.rpc.api.permissions.SpacePermission;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.GroupFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.component.dialog.MacroBrowserDialog;
import com.atlassian.confluence.webdriver.pageobjects.page.NoOpPage;
import it.com.atlassian.confluence.plugins.webdriver.AbstractJiraIssueMacroTest;
import it.com.atlassian.confluence.plugins.webdriver.helper.ApplinkHelper;
import it.com.atlassian.confluence.plugins.webdriver.pageobjects.WarningAppLinkDialog;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JiraIssuesNonExistentAppIDTest extends AbstractJiraIssueMacroTest
{
    @Test
    public void shouldReturnErrorWhenAppLinkDoesNotExist() throws IOException
    {
        String confluenceBase = System.getProperty("baseurl.confluence");
        String url = confluenceBase + "/plugins/servlet/issue-retriever?appId=dba636ee-7493-41cf-9f12-0b5abe376523&url="
                + confluenceBase
                + "&columns=summary"
                + getAuthQueryString();
        assertReturnErrorWithMessage(url, "Could not find the application link with the appId");
    }

    @Test
    public void shouldReturnErrorWhenURLParameterIsEmpty() throws IOException
    {
        String confluenceBase = System.getProperty("baseurl.confluence");
        String url = confluenceBase + "/plugins/servlet/issue-retriever?appId="
                + testAppLinkId
                + "&columns=summary"
                + getAuthQueryString();
        assertReturnErrorWithMessage(url, "The url parameter is empty");
    }

    @Test
    public void shouldReturnErrorWhenURLNotInSameOrigin() throws IOException
    {
        String confluenceBase = System.getProperty("baseurl.confluence");
        String url = confluenceBase + "/plugins/servlet/issue-retriever?appId="
                + testAppLinkId
                + "&url=http://whatever"
                + "&columns=summary"
                + getAuthQueryString();
        assertReturnErrorWithMessage(url, "Could not find the application link with the provided url");
    }

    @Test
    public void shouldReturnErrorWhenURLIsInvalid() throws IOException
    {
        String confluenceBase = System.getProperty("baseurl.confluence");
        String url = confluenceBase + "/plugins/servlet/issue-retriever?appId="
                + testAppLinkId
                + "&url=invalid:invalid//whatever"
                + "&columns=summary"
                + getAuthQueryString();
        assertReturnErrorWithMessage(url, "The url parameter is invalid");
    }

    private void assertReturnErrorWithMessage(String url, String message) throws IOException
    {
        GetMethod getMethod = new GetMethod(url);
        final int status = client.executeMethod(getMethod);
        Assert.assertEquals(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, status);
        Assert.assertTrue(getMethod.getResponseBodyAsString().contains(message));
    }
}
